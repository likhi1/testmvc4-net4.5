﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
///////////////// Addition //////////
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 


namespace TestMVC4.Models.ViewModel
{
    public class TblEmployeeVM
    {
        [Key]
        [Display(Name = "No.")]
        public int EmployeeId { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-ชื่อ")]
        [Display(Name = "ชื่อ")]
        public string Name { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-เพศ")]
        [Display(Name = "เพศ")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-เมือง")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "เมือง")]
        public string City { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-รหัสแผนก")]
        [Display(Name = "รหัสแผนก")] 
        public Nullable<int> DepartmentId { get; set; }


        //\\\\\\\\\\\\\\\\\\\\\\\\\\  Sample Validate Control \\\\\\\\\\\\\\\\
        [Required(ErrorMessage = "กรุณาระบุ-อีเมล์")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "อีเมล์")]
        public string Email { get; set; }

        [Display(Name = "วัน-เวลา")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d.MMM.yyyy  HH:mm }")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> InputDate { get; set; }



        public virtual TblDepartment tblDepartment { get; set; } 
    }
}