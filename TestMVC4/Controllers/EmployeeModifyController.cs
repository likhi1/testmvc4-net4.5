﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestMVC4.Models;
///////////////// Addition //////////
using TestMVC4.Models.ViewModel; 

namespace TestMVC4.Controllers
{
    public class EmployeeModifyController : Controller
    {

        private dbTestmvc442Entities db = new dbTestmvc442Entities();

        //
        // GET: /EmployeeModify/

        public ActionResult Index()
        {
            var viewModel = db.TblEmployees
                .OrderByDescending(x => x.EmployeeId)
                .ThenBy(x => x.Name)
                .Select(  x => new TblEmployeeVM {
                        EmployeeId = x.EmployeeId ,
                        Name = x.Name ,
                        Gender = x.Gender ,
                        City = x.City,
                        DepartmentId = x.DepartmentId 
                });
            return View(viewModel);

        }

        

        //
        // GET: /EmployeeModify/Create

        public ActionResult Create()
        {
            TblEmployeeVM viewModel = new TblEmployeeVM();
            return View(viewModel);
        }

        //
        // POST: /EmployeeModify/Create

        [HttpPost]
        public ActionResult Create(TblEmployee tblemployee)
        {
            if (ModelState.IsValid)
            {
                db.TblEmployees.Add(tblemployee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblemployee);
        }

        //
        // GET: /EmployeeModify/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TblEmployee tblemployee = db.TblEmployees.Find(id);

            if (tblemployee == null)
            {
                return HttpNotFound();
            }

            TblEmployeeVM viewmodel = new TblEmployeeVM
            {
                EmployeeId = tblemployee.EmployeeId ,
                Name = tblemployee.Name ,
                Gender = tblemployee.Gender ,
                City = tblemployee.City ,
                DepartmentId = tblemployee.DepartmentId 
            };


            return View(viewmodel);
        }

        //
        // POST: /EmployeeModify/Edit/5

        [HttpPost]
        public ActionResult Edit(TblEmployeeVM viewmodel)
        {  
            TblEmployee tblemployee = db.TblEmployees.Find( viewmodel.EmployeeId);

            if (ModelState.IsValid)
            {
                tblemployee.EmployeeId = viewmodel.EmployeeId ;
                tblemployee.Name = viewmodel.Name ;
                tblemployee.Gender = viewmodel.Gender ;
                tblemployee.City = viewmodel.City ;
                tblemployee.DepartmentId = viewmodel.DepartmentId ;
                 
                //////////// Save To Database  
                db.Entry(tblemployee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            return View(viewmodel);
        }

  
        //
        // POST: /EmployeeModify/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TblEmployee tblemployee = db.TblEmployees.Find(id);
            db.TblEmployees.Remove(tblemployee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}